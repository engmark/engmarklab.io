---
title: Nightly Rust development with Nix
categories:
  - Nix
  - Rust
  - development
  - IDE
---

This one took a while to get simple enough; enjoy!

## Background

While
[getting started with Bevy](https://bevyengine.org/learn/quick-start/getting-started/)
I wanted to make sure I had an easily reproducible development environment by
using only Nix as the package manager. However, the latest version of Bevy was
not compatible with stable Rust, so looked into Nix development environments for
the nightly version. Unfortunately the instructions I could find either
recommended using Rustup (that is, involving another package manager and
probably sacrificing reproducibility), were "left as an exercise to the
reader"-style vague, or looked complex to maintain. I also wanted to start out
with stable integration with JetBrains IDEA, my IDE of choice, so that when I
upgrade any part of the setup it would have a good chance to Just Keep
Working™.

I tried starting from a standard Nix shell, but nightly Rust integration seemed
like it would need one of the aforementioned complex setups or some third party
involvement. I didn't want to add another framework, but finally caved and
started using one. This happily turned out to be the right choice.

## Prerequisites

- [Nix](https://nixos.org/)
- [devenv](https://devenv.sh/) (`pkgs.devenv`)
- [direnv](https://direnv.net/) (`programs.direnv.enable = true` in NixOS)
- Optionally [RustRover](https://www.jetbrains.com/rust/)
  (`pkgs.jetbrains.rust-rover`) or
  [JetBrains IDEA Ultimate](https://www.jetbrains.com/idea/)
  (`pkgs.jetbrains.idea-ultimate`) with the official Rust plugin[^tested-ide]

[^tested-ide]:
    I've only tested this with IDEA, but it should work similarly in RustRover.

## Setup

1. Go to your project directory
1. Run `devenv init` to create devenv and direnv configuration
1. Run `devenv inputs add fenix github:nix-community/fenix --follows nixpkgs` to
   add nightly Rust support
1. Add the following in `devenv.nix`:

   ```nix
   languages.rust = {
     channel = "nightly";
     components = [
       "cargo"
       "rust-src"
       "rustc"
     ];
     enable = true;
   };
   ```

At this point `cargo --version` and `rustc --version` should print `-nightly`
version numbers, and you can run `cargo init` to start the Rust journey.

## Optional IDE setup

Start your IDE inside the project directory[^run-idea] to have all the tools you
just installed available on the path. In _Languages & Frameworks_ → _Rust_, you
can now set the relevant paths for the project:

- _Toolchain location:_ <code><em>[path
  to project]</em>/.devenv/profile/bin</code>
- _Standard library:_ <code><em>[path
  to project]</em>/.devenv/profile/lib/rustlib/src/rust/library</code>

This should be enough to build and run the code, and to follow references to
Rust core, within the IDE.

[^run-idea]:
    I use `nohup idea-ultimate nosplash . &>/dev/null & disown` to be able to
    use the shell immediately after starting IDEA.
