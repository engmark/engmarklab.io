---
title: Consistent fonts in Firefox
categories:
  - fonts
  - design
  - web
  - Firefox
---

Are you sick of every single website using different fonts in the name of
branding? You can easily force websites to use the browser default fonts:

1. <q>Preferences</q> → <q>Language and Appearance</q> → <q>Fonts and Colors</q>
   → <q>Advanced</q>
2. Un-check <q>Allow pages to choose their own fonts, instead of your selections
   above</q>

Alternatively (thanks to Ron for pointing this out) you can
[set `browser.display.use_document_fonts` to `0`](https://kb.mozillazine.org/About:config_entries#Browser.)
to <q>Never use document's fonts</q>. You can do this in
[about:config](about:config), or by setting it in your Firefox profile
`prefs.js`, or, if you're using NixOS, by
[setting it in `programs.firefox.preferences`](https://gitlab.com/engmark/root/-/blob/a99527f899c14abddd11f0d7a01543d30d4a481d/configuration.nix#L440).

Pros:

- Most websites are easier to read.
- Avoids downloading large font files. A decent page of text is single-digit
  kilobytes, but the fonts on a typical ~2021 website weighs in at over
  [100 kilobytes](https://httparchive.org/reports/page-weight?lens=top1m&start=2020_12_01&end=2021_12_01&view=list#bytesFont)
  over
  [4 requests](https://httparchive.org/reports/page-weight?lens=top1m&start=2020_12_01&end=2021_12_01&view=list#reqFont).

Cons:

- Some websites have over-specified their layout, so things don't align properly
  when laid out with another font. This probably means the site won't work well
  in a lot of situations (niche browsers, ad blockers, without images, basically
  anything other than the browser the web developer is using), so it might be
  worth talking to the owners if you use the site often.
- Some websites use icon fonts without fallbacks, so you might see one of those
  ugly
  [`.notdef` glyphs](https://en.wikipedia.org/w/index.php?title=Unicode_input&oldid=1066125550#Availability)
  instead of an icon.
- It
  [doesn't always work](https://bugzilla.mozilla.org/show_bug.cgi?id=1674508).
