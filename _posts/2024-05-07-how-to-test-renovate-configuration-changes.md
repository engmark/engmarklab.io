---
title: How to test Renovate configuration changes in GitLab CI
categories:
  - Renovate
  - GitLab
  - testing
---

You've changed `renovate.json`, but of course you want to test the change before
merging. There doesn't seem to be a simple guide to this, so here's what I've
cobbled together.

## Prerequisites

- A working [Renovate Runner](https://gitlab.com/renovate-bot/renovate-runner/)
  setup, for example [my repository](https://gitlab.com/engmark/renovate). (The
  steps below assume a GitLab setup, but should work with minor modifications on
  other platforms.)
- A branch with a `renovate.json` file. For example, [my first attempt at
  `regexManagers`][1].

## Journey

Feel free to [skip this section](#steps) if you're only interested in the actual
instructions.

### Goals

Aside from testing the `renovate.json` change before merging it, there are a few
other things which would be good to achieve:

1. Minimal or no temporary changes to the `renovate.json` branch for testing
   purposes. Any changes introduced to the branch increases risk, for example in
   the form of accidentally merging the temporary changes, the changes breaking
   something else, or introducing an error when reverting the temporary changes
   after testing.
1. Minimal or no changes (permanent or temporary) to the Renovate Runner setup
   to enable testing. This improves the chances that the testing doesn't
   interfere with normal operation.
1. The test run should be as similar as possible to the normal GitLab CI runs as
   possible.

### Challenges

1. Renovate Runner by default only runs when triggered from a schedule.
1. Running the test locally would be great, but I can't easily replicate the
   GitLab CI setup exactly. Because of the risk of differences and the stronger
   test result of running it in GitLab CI I've chosen to only attempt running in
   GitLab CI.
1. The behaviour I'm looking for in Renovate requires it to
   [create a new branch and associated merge request for an already up-to-date dependency](https://github.com/renovatebot/renovate/discussions/22477).

## Steps

1. Deactivate any existing Renovate Runner schedules, to avoid interference.
1. Create a new Renovate Runner schedule with the following settings:

   - Any schedule. `0 0 1 1 0`,
     [next combination of midnight, Sunday, January 1st](https://crontab.guru/#0_0_1_1_0),
     is probably rare enough not to cause any issues even if it's accidentally
     activated.
   - Set the
     [`RENOVATE_REPOSITORIES` variable](https://docs.renovatebot.com/self-hosted-configuration/#repositories),
     the qualified name of the repository you want to test. In case of [the
     branch above][1] this should be "engmark/mypy-exercises".
   - Set the
     [`RENOVATE_BASE_BRANCHES` variable](https://docs.renovatebot.com/configuration-options/#basebranches)
     to the name of the branch with the Renovate configuration change you want
     to test. In case of [the branch above][1] this should be
     "feat/enable-updating-readme-link".
   - Disable auto-discovery, so that Renovate doesn't ignore
     `RENOVATE_REPOSITORIES`, and use dry run mode to avoid actually creating
     any branches and merge requests. These can be set separately, but to avoid
     clutter we can set the
     [`RENOVATE_EXTRA_FLAGS` variable](https://gitlab.com/renovate-bot/renovate-runner/-/blob/main/README.md#configure-cicd-variables)
     to "--autodiscover=false --dry-run=full".
   - Make sure the schedule is _deactivated,_ since this should only be run
     manually.

   The resulting schedule should look a bit like this:
   ![Renovate configuration change test schedule form](/assets/images/renovate-configuration-change-test-schedule.png)

1. Manually run the new schedule, and verify the output:
   - Renovate ran on only the relevant repository and branch.
   - Renovate used the new configuration as expected.
1. Re-activate any Renovate Runner schedules deactivated in step 1.

If everything went as expected it should be safe to merge the `renovate.json`
change.

[1]: https://gitlab.com/engmark/mypy-exercises/-/merge_requests/392/diffs
