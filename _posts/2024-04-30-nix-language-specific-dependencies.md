---
title: Nix language-specific dependencies
categories:
  - Nix
  - Linux
  - software
---

The following Nix shell declaration has a
[common bug](https://wiki.nixos.org/wiki/Python#My_module_cannot_be_imported):

```nix
{% include shell-broken-interpreter-dependency.nix -%}
```

Running `python` within the shell works, and running `pytest` works, but the two
are not connected:

```console
$ python -c "import pytest"
Traceback (most recent call last):
  File "<string>", line 1, in <module>
ModuleNotFoundError: No module named 'pytest'
```

The fix is to explicitly include the Python packages with Python itself:

```nix
{% include shell-fixed-interpreter-dependency.nix -%}
```

Many interpreters support this pattern. Have a look for "`withPackages`" in the
[nixpkgs manual](https://nixos.org/manual/nixpkgs/).
