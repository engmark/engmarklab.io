---
title: The lost taglines
categories:
  - documentation
  - silliness
  - software
---

Software documentation usually does not include the most important facts you
want to know as a beginner. The kind of stuff you might internalise after using
something for years, but you _could not_ have learned by reading the
documentation. The stuff which should show up as a blinking orange & teal banner
on top of the documentation page — the lost taglines:

- Bash: "Use More Quotes™!"
- CSV: Your data _will_ contain commas.
- Git: `bisect` — you'll never be the same again.
- JSON: Add a schema!
- Markdown: The parser is the limiting factor.
- Nix: It's worth the pain.
- PHP: The comments are the most useful parts of the docs.
- Perl: The first thing you must do is add every safety pragma.
- Python: `mypy --strict` and `ruff` FTW.
- Rust: The compiler loves you, and only wants you to be happy.
- vCard: v3 is the only one supported anywhere.
- YAML: Have you considered JSON?
