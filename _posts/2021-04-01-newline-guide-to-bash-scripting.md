---
title: <code>echo 'I wrote a Bash book!' &gt;&gt; /dev/world</code>
categories: programming bash book
author: Victor Engmark
---

I wrote a book —
[The newline Guide to Bash Scripting](https://www.newline.co/courses/newline-guide-to-bash-scripting)!
I'll let the intro speak for itself:

> It’s aimed at developers who want to get the job done right the first time,
> and make sure maintenance is a breeze. Topics include:
>
> - how to find help on arbitrary commands
> - how to structure a robust and maintainable script
> - editing commands and interacting with the terminal efficiently
> - tools for dealing with version control, JSON, XML, text encodings, images
>   and compressed files
> - quality assurance
> - … and much more
>
> […]
>
> Bash is _the_ Linux glue language, so this book is aimed at software
> developers mainly working in some other language. We assume that you have
> working knowledge of programming concepts like variable assignments, loops and
> files, and at least a passing familiarity with interactive shells.
>
> […]
>
> This book was written using Bash 4.2 and 5.0 on Linux. The majority of the
> contents should be applicable to versions long before and after those, and to
> other Unix–like operating systems. However, since _everything_ on Linux is
> configurable, absolute statements such as “$PATH will not be defined in a
> crontab” should be treated as a pragmatic shorthand for providing a virtual
> machine with the configuration I used when writing. It is not even
> theoretically possible to write a piece of software which will behave the same
> no matter how and where it is run. So the only way to _know_ what some command
> will actually do is to run it, and no statement in this book should be treated
> as absolute truth. In the same vein, the code in this book is written to make
> a best effort at doing the right thing in a reasonable set of circumstances.

I'm proud to have written this book. I would've been super happy if it sold five
copies, so I was pleasantly surprised to find it sold over 100 copies in the
first week. Allow me to finish with the acknowledgements:

> Thank you to Nate Murray, my publisher, for his advice and endless patience,
> Gillian Merchant for her excellent review of later revisions, my partner for
> discussions about form and content, all my encouraging friends, family,
> colleagues at Catalyst and Toitū Te Whenua, and Andrew Maguire and John
> Billings for reviewing an early draft of the book. Special thanks to all the
> Bash community contributors on Stack Overflow, Unix & Linux Stack Exchange and
> Greg’s Wiki, which are treasure troves of information about all things Bash.
