---
title: Elden Ring Linux performance settings
categories:
  - Elden Ring
  - game
  - Linux
  - performance
---

tl;dr Just set everything to max performance in
[CoreCtrl](https://gitlab.com/corectrl/corectrl) and you're good to go.

Internet advice is usually of the form "X worked for me." It's extremely rare to
get an explanation _why_ it should work, a link to documentation, or actual
measurements. After seeing a bunch of Internet advice to improve the performance
of Elden Ring on Linux I decided to just try some of them and _actually
[benchmark](https://danluu.com/why-benchmark/)._ The result was surprising, but
comes with the usual caveats: my hardware, firmware, and software are probably
different from yours, things change fast, and any of this might be obsolete
before it's published.

## Specs

```console
$ inxi --cpu --graphics
CPU:       Info: 6-Core model: Intel Core i7-8700 bits: 64 type: MT MCP cache: L2: 12 MiB
           Speed: 4508 MHz min/max: 800/4600 MHz Core speeds (MHz): 1: 4508 2: 4564 3: 4604 4: 4362 5: 4510 6: 4568 7: 4572
           8: 4415 9: 4338 10: 4401 11: 4402 12: 4401
Graphics:  Device-1: Advanced Micro Devices [AMD/ATI] Navi 14 [Radeon RX 5500/5500M / Pro 5500M] driver: amdgpu v: kernel
           Device-2: N/A type: USB driver: snd-usb-audio,uvcvideo
           Display: x11 server: X.Org 1.20.13 driver: loaded: amdgpu unloaded: fbdev,modesetting,vesa resolution:
           1: 3840x2160~60Hz 2: 1920x1080~60Hz
           OpenGL: renderer: AMD Radeon RX 5500 XT (NAVI14 DRM 3.42.0 5.15.32 LLVM 13.0.1) v: 4.6 Mesa 21.3.7
```

So an Intel i7-8700, AMD Radeon RX 5500, an extra "screen" which is really just
an audio receiver, and the latest stable drivers.

## Software

- [NixOS](https://nixos.org/) unstable
- Steam stable
- GNOME on Wayland
- Various other software running in the background, like a browser, email client
  and IDE

## Steam game settings

- Compatibility set to Proton Experimental
- Launch options set as mentioned below, always enabling
  [Mango HUD](https://github.com/flightlessmango/MangoHud) for an FPS counter

## Game graphics settings

- Fullscreen
- 1920×1080 resolution
- Auto-detect best rendering settings off
- All graphics settings at "high"

## Methodology

1. Load latest save
1. While standing in the same spot, turn the camera around in all sorts of
   directions
1. Note the _lowest FPS_ which shows up in the Mango HUD in about 10 seconds

## Results

1. No extra options, just `MANGOHUD=1 %command%`: 35 FPS
1. [`gamemoderun MANGOHUD=1 %command%`](https://github.com/FeralInteractive/gamemode):
   33 FPS
1. [`WINE_FULLSCREEN_FSR=1 MANGOHUD=1 %command%`](https://www.amd.com/en/technologies/fidelityfx-super-resolution):
   34 FPS
1. `AMD_VULKAN_ICD=RADV MANGOHUD=1 %command%`: 33 FPS (no reference, sorry)
1. `ENABLE_VKBASALT=1 MANGOHUD=1 %command%`: 34 FPS (no reference, sorry)
1. `VKD3D_CONFIG=no_upload_hvv,single_queue MANGOHUD=1 %command%`: 31 FPS (no
   reference, sorry)

Doesn't get much clearer than that: none of these make a significant difference
with my setup.

## Things I _didn't_ test:

- After tweaking the CoreCtrl settings I saw no appreciable difference between
  X11 and Wayland performance, so I've only included Wayland measurements here.
  If you know of any reason why either of them should perform _significantly_
  better than the other for any specific settings, please let me know.
- With a different FPS counter, since I assume its overhead is unnoticeable.
- Disconnecting the receiver, since it's necessary to get sound.
- [`DXVK` options won't do anything since this isn't a DX12 game](https://www.reddit.com/r/linux_gaming/comments/t2o67v/did_anyone_manage_to_get_the_fps_counter_to_work/).
- Other desktop environments, since I don't expect them to make significant
  difference.
- [`PROTON_USE_SECCOMP`](https://github.com/ValveSoftware/Proton) is long
  obsolete.
