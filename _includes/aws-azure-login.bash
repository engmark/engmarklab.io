aws-azure-login() {
    echo "Select profile, or press Ctrl-c to cancel."
    select AWS_PROFILE in $(
        grep --null-data --only-matching --perl-regexp --regex='(?<=\[profile )([^\]]+)(?=\])(?!\]\nsource_profile=)' ~/.aws/config |
            tr '\0' '\n'
    ); do
        export AWS_PROFILE
        break
    done

    command aws-azure-login --profile="$AWS_PROFILE" "$@"

    local sub_profiles
    mapfile -t sub_profiles < <(
        grep --null-data --only-matching --perl-regexp --regex='(?<=\[profile )([^\]]+)(?=\]\nsource_profile='"$AWS_PROFILE"'\n)' ~/.aws/config |
            tr '\0' '\n'
    )

    if (("${#sub_profiles[@]}" != 0)); then
        echo "Select sub-profile, or press Ctrl-c to keep ${AWS_PROFILE}."
        select AWS_PROFILE in "${sub_profiles[@]}"; do
            break
        done
    fi
}
