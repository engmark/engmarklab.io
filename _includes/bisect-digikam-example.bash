#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

# Reusable & reproducible build command; will not rebuild the second time around
build=(nix-build --attr digikam --no-out-link)
"${build[@]}" || exit 125

# If "ExifTool process cannot be started" appears on standard error within 10
# seconds, this is a bad commit.
# ShellCheck insists that the *whole* command exit code must be negated.
{
    if {
        timeout 10s "$("${build[@]}")/bin/digikam" 3>&2 2>&1 1>&3- || [[ $? -eq 124 ]]
    } | tee -p /dev/fd/4 | grep --quiet 'ExifTool process cannot be started'; then
        false
    else
        true
    fi
} 3>&2 2>&1 1>&3- 4>&1
