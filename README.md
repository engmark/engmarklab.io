# [Paperless blog](https://paperless.blog/)

![Build Status](https://gitlab.com/engmark/engmark.gitlab.io/badges/master/pipeline.svg)

## Local build

```bash
./.gitlab/build.bash
```

## Development setup

Requirements:

- [`pre-commit`](https://pre-commit.com/)
- `ruby`

Optionally install pre-commit hooks:

```bash
pre-commit install --hook-type=commit-msg --hook-type=pre-commit --overwrite
```

## Tests

Either see the [`test JavaScript` job](.gitlab-ci.yml) or open
[`test/index.html`](test/index.html) using a local web server.

## Serve local copy

```bash
jekyll serve --livereload
```

With only the last post:

```bash
jekyll serve --limit_posts=1 --livereload
```

## Upstream documentation

- [GitLab CI](https://docs.gitlab.com/ce/ci/)
- [GitLab Pages](https://docs.gitlab.com/ce/user/project/pages/)
- [Jekyll](https://jekyllrb.com/docs/)
